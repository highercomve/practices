// Add any extra import statements you may need here


// Add any helper functions you may need here
function composeIndex (j, i) {
  return j < i ? `${j}${i}` : `${i}${j}`
} 

function numberOfWays(arr, k) {
  // Write your code here
  const valuesIndexes = arr.reduce((acc, value, index) => {
    acc[value] = acc[value] === undefined ? [index] : acc[value].concat([index])
    return acc
  }, {})
  
  const convinationsFound = arr.reduce((acc, value, index) => {
    const needIt = k - value
    if (valuesIndexes[needIt] === undefined) {
      return acc
    }
    
    valuesIndexes[needIt].forEach((i) => {
      if (i !== index && acc[composeIndex(index, i)] === undefined) {
        acc.total += 1
        acc[composeIndex(index, i)] = true
      } 
    })
    return acc
  }, { total: 0 })

  return convinationsFound.total
}











// These are the tests we use to determine if the solution is correct.
// You can add your own at the bottom, but they are otherwise not editable!
function printInteger(n) {
  var out = '[' + n + ']';
  return out;
}

var test_case_number = 1;

function check(expected, output) {
  var result = (expected == output);
  var rightTick = "\u2713";
  var wrongTick = "\u2717";
  if (result) {
    var out = rightTick + ' Test #' + test_case_number;
    console.log(out);
  }
  else {
    var out = '';
    out += wrongTick + ' Test #' + test_case_number + ': Expected ';
    out += printInteger(expected);
    out += ' Your output: ';
    out += printInteger(output);
    console.log(out);
  }
  test_case_number++;
}

var k_1 = 6;
var arr_1 = [1, 2, 3, 4, 3];
var expected_1 = 2;
var output_1 = numberOfWays(arr_1, k_1);
check(expected_1, output_1);

var k_2 = 6;
var arr_2 = [1, 5, 3, 3, 3];
var expected_2 = 4;
var output_2 = numberOfWays(arr_2, k_2);
check(expected_2, output_2);

// Add your own test cases here

var k_2 = 5;
var arr_2 = [1,4,4,4,4,2,3,3,2];
var expected_2 = 8;
var output_2 = numberOfWays(arr_2, k_2);
check(expected_2, output_2);
