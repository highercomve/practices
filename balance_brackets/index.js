// Add any extra import statements you may need here


// Add any helper functions you may need here


function isBalanced(s) {
  // Write your code here
  const stack = []
  const brakets = "{([])}"
  
  for (var i = 0; i < s.length; i++) {
    const index = brakets.indexOf(s.charAt(i))
    if (index >= 0) {
      stack.push(s.charAt(i))
    }
    if (index >= 3 && index <= 5) {
      const closeChar = stack.pop()
      const openChar = stack.pop()
      if (brakets.charAt(brakets.length - brakets.indexOf(openChar) - 1) !== closeChar) {
        return false
      }
    }
  }
  
  return true
}










// These are the tests we use to determine if the solution is correct.
// You can add your own at the bottom, but they are otherwise not editable!
function printString(str) {
  var out = '["' + str + '"]';
  return out;
}

var test_case_number = 1;

function check(expected, output) {
  var result = (expected == output);
  var rightTick = "\u2713";
  var wrongTick = "\u2717";
  if (result) {
    var out = rightTick + ' Test #' + test_case_number;
    console.log(out);
  }
  else {
    var out = '';
    out += wrongTick + ' Test #' + test_case_number + ': Expected ';
    out += printString(expected);
    out += ' Your output: ';
    out += printString(output);
    console.log(out);
  }
  test_case_number++;
}

var s_1 = "{[(])}";
var expected_1 = false;
var output_1 = isBalanced(s_1);
check(expected_1, output_1);

var s_2 = "{{[[(())]]}}";
var expected_2 = true;
var output_2 = isBalanced(s_2);
check(expected_2, output_2);

// Add your own test cases here
var s_2 = "arr.reduce((acc, v) => { return acc + v }, 0)";
var expected_2 = true;
var output_2 = isBalanced(s_2);
check(expected_2, output_2);

var s_2 = ")";
var expected_2 = false;
var output_2 = isBalanced(s_2);
check(expected_2, output_2);

var s_2 = "blah";
var expected_2 = true;
var output_2 = isBalanced(s_2);
check(expected_2, output_2);