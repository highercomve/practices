#!/bin/bash

set -e

for d in ${PWD}/cases/*/ ; do
  echo "Running Case $d"
  output_file="${d}output.txt"

  cat "${d}input.txt" | OUTPUT_PATH=$output_file go run .
  expected=`cat "${d}expected.txt"`
  output=`cat $output_file`

  if [ "$output" == "$expected" ]; then
    echo "Test Passed :)"
  else 
    echo "expected: " >&2
    echo "$expected" >&2
    echo "output: " >&2
    echo "$output" >&2
    echo "Test Failed" >&2
    exit 1
  fi
done
