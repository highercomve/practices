package main

import (
	"bufio"
	"fmt"
	"io"
	"math"
	"os"
	"strconv"
	"strings"
)

// Easy version that of thinking but doesn't perform that good.
func ArrayManipulation2(n int32, queries [][]int32) int64 {
	// Write your code here
	arr := make([]int64, n)
	max := int64(0)
	for _, query := range queries {
		from := query[0]
		to := query[1]
		val := int64(query[2])
		for i := from - 1; i < to; i++ {
			newVal := arr[i] + val
			arr[i] = newVal
			if newVal > max {
				max = newVal
			}
		}
	}

	return max
}

/*
 * Complete the 'arrayManipulation' function below.
 *
 * The function is expected to return a LONG_INTEGER.
 * The function accepts following parameters:
 *  1. INTEGER n
 *  2. 2D_INTEGER_ARRAY queries
 */
func arrayManipulation(n int32, queries [][]int32) int64 {
	// Write your code here
	arr := make([]int64, n)
	max := int64(0)
	sum := int64(0)
	for _, query := range queries {
		from := query[0]
		to := query[1]
		val := int64(query[2])

		arr[from-1] += val
		if to < n {
			arr[to] -= val
		}
	}

	for _, v := range arr {
		sum += v
		max = int64(math.Max(float64(sum), float64(max)))
	}
	return max
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 16*1024*1024)

	stdout, err := os.Create(os.Getenv("OUTPUT_PATH"))
	checkError(err)

	defer stdout.Close()

	writer := bufio.NewWriterSize(stdout, 16*1024*1024)

	firstMultipleInput := strings.Split(strings.TrimSpace(readLine(reader)), " ")

	nTemp, err := strconv.ParseInt(firstMultipleInput[0], 10, 64)
	checkError(err)
	n := int32(nTemp)

	mTemp, err := strconv.ParseInt(firstMultipleInput[1], 10, 64)
	checkError(err)
	m := int32(mTemp)

	var queries [][]int32
	for i := 0; i < int(m); i++ {
		queriesRowTemp := strings.Split(strings.TrimRight(readLine(reader), " \t\r\n"), " ")

		var queriesRow []int32
		for _, queriesRowItem := range queriesRowTemp {
			queriesItemTemp, err := strconv.ParseInt(queriesRowItem, 10, 64)
			checkError(err)
			queriesItem := int32(queriesItemTemp)
			queriesRow = append(queriesRow, queriesItem)
		}

		if len(queriesRow) != 3 {
			panic("Bad input")
		}

		queries = append(queries, queriesRow)
	}

	result := arrayManipulation(n, queries)

	fmt.Fprintf(writer, "%d\n", result)

	writer.Flush()
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
