'use strict';

const fs = require('fs');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', function(inputStdin) {
    inputString += inputStdin;
});

process.stdin.on('end', function() {
    inputString = inputString.split('\n');

    main();
});

function readLine() {
    return inputString[currentLine++];
}

/*
 * Complete the 'countInversions' function below.
 *
 * The function is expected to return a LONG_INTEGER.
 * The function accepts INTEGER_ARRAY arr as parameter.
 */

function countInversions(arr) {
    // Write your code here
		const g = new Graphs()
		const id = arr.join('')
		const base = new Node(id)
		g.nodes.set(id, base)
		permutate(arr, base, 0, arr.length, g, {})
	
		const visited = new Map()
		const sortedId = arr.sort((a, b) => a - b).join('')
		const queue = [base]
		
		const dist = {[id]: 0}
		while (queue.length > 0) {
			const node = queue.shift()
			for ([k, n] of node.adj) {
				if (visited.has(n.id)) {
					continue
				}
		
				visited.set(n.id, true)
				dist[n.id] = dist[node.id] + 1
				queue.push(n)
	
				if (n.id === sortedId) {
					return dist[n.id]
				}
			}
		}
		
		return dist[sortedId]
}


function Graphs() {
  this.nodes = new Map()  
}

Graphs.prototype.add = function (node) {
  this.nodes.set(node.id, node)
} 

Graphs.prototype.addEdge = function (source, destination) {
  if (!this.nodes.has(source)) {
    this.nodes.set(source, new Node(source))
  }
  if (!this.nodes.has(destination)) {
    this.nodes.set(destination, new Node(destination))
  }
  
  const a = this.nodes.get(source)
  const b = this.nodes.get(destination)
  b.parent = a
  a.add(b)
}


Graphs.prototype.getNode = function (id) {
  return this.nodes.get(id)
}

function Node(id, parent) {
  this.id = id
  this.parent = parent
  this.adj = new Map()
}

Node.prototype.add = function (node) {
  this.adj.set(node.id, node)
}

const permutate = (arr, parent, start, end, graph, memo = {}) => {
  if (start >= end || end < 0 || end <= start) {
    return
  }

  const arrID = arr.join('')
  const subArr = arr.slice(0, start)
    .concat(arr.slice(start, end + 1).reverse())
    .concat(arr.slice(end + 1, arr.length))

  const id = subArr.join('')
  graph.addEdge(parent.id, id)
  if (memo[arrID+id]) {
    return graph.nodes.get(memo[arrID+id])
  } else {
    memo[arrID+id] = id
  }


  const initialI = start
  const endI = end
  for (let i = initialI; i < endI; i++) {
    permutate(arr, parent, i + 1, end, graph, memo)
    permutate(arr, parent, 0, end - i - 1, graph, memo)
    permutate(arr, parent, i, i + 1, graph, memo)
  }

  return permutate(subArr, graph.nodes.get(id), 0, subArr.length, graph, memo)
}


function main() {
    const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

    const t = parseInt(readLine().trim(), 10);

    for (let tItr = 0; tItr < t; tItr++) {
        const n = parseInt(readLine().trim(), 10);

        const arr = readLine().replace(/\s+$/g, '').split(' ').map(arrTemp => parseInt(arrTemp, 10));

        const result = countInversions(arr);

        ws.write(result + '\n');
    }

    ws.end();
}
