package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

/*
 * Complete the 'countInversions' function below.
 *
 * The function is expected to return a LONG_INTEGER.
 * The function accepts INTEGER_ARRAY arr as parameter.
 */
func countInversions(arr []int32) int64 {
	// Write your code here
	_, count := mergeSort(arr, int64(0))

	return count
}

func mergeSort(arr []int32, count int64) ([]int32, int64) {
	if len(arr) <= 1 {
		return arr, 0
	}

	middle := len(arr) / 2

	left, a := mergeSort(arr[:middle], count)
	right, b := mergeSort(arr[middle:], count)

	return merge(left, right, a+b)
}

func merge(left []int32, right []int32, count int64) ([]int32, int64) {
	result := make([]int32, 0, len(left)+len(right))
	location := make([]int64, len(left)+len(right))

	i := 0
	for len(left) > 0 || len(right) > 0 {
		if len(left) == 0 {
			result = append(result, right...)
			break
		}

		if len(right) == 0 {
			result = append(result, left...)
			break
		}

		if left[0] <= right[0] {
			result = append(result, left[0])
			left = left[1:]
			location[i] = 0
		} else {
			result = append(result, right[0])
			right = right[1:]
			location[i] = int64(len(left))
		}
		i++
	}

	for i := 0; i < len(location); i++ {
		count += location[i]
	}

	return result, count
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 16*1024*1024)

	stdout, err := os.Create(os.Getenv("OUTPUT_PATH"))
	checkError(err)

	defer stdout.Close()

	writer := bufio.NewWriterSize(stdout, 16*1024*1024)

	tTemp, err := strconv.ParseInt(strings.TrimSpace(readLine(reader)), 10, 64)
	checkError(err)
	t := int32(tTemp)

	for tItr := 0; tItr < int(t); tItr++ {
		nTemp, err := strconv.ParseInt(strings.TrimSpace(readLine(reader)), 10, 64)
		checkError(err)
		n := int32(nTemp)

		arrTemp := strings.Split(strings.TrimSpace(readLine(reader)), " ")

		var arr []int32

		for i := 0; i < int(n); i++ {
			arrItemTemp, err := strconv.ParseInt(arrTemp[i], 10, 64)
			checkError(err)
			arrItem := int32(arrItemTemp)
			arr = append(arr, arrItem)
		}

		result := countInversions(arr)

		fmt.Fprintf(writer, "%d\n", result)
	}

	writer.Flush()
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
