#!/bin/bash

set -e

function run_test() {
  echo "Running Case ${1}"
  output_file="${1}output.txt"

  cat "${1}input.txt" | OUTPUT_PATH=$output_file go run .
  expected=`cat "${1}expected.txt"`
  output=`cat $output_file`

  if [ "$output" == "$expected" ]; then
    echo "Test Passed :)"
  else 
    echo "expected: " >&2
    echo "$expected" >&2
    echo "output: " >&2
    echo "$output" >&2
    echo "Test Failed" >&2
    exit 1
  fi
}

if [ "$1" != "" ]; then
  run_test ${PWD}/cases/$1/
else
  for d in ${PWD}/cases/*/ ; do
    run_test $d
  done
fi 
