package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"slices"
	"strconv"
	"strings"
)

func searchMaximun(queue map[int32][]int) int32 {
	max := int32(-1)
	for k, v := range queue {
		if len(v) >= 2 && k > max {
			max = k
		}
	}

	return max
}

func processQueue(priority []int32) []int32 {
	queue := map[int32][]int32{}
	elementPos := map[int32][]int{}
	priorityMap := map[int]int32{}
	maxPriority := int32(-1)
	for k, v := range priority {
		priorityMap[k] = v
		if _, ok := elementPos[v]; ok {
			elementPos[v] = append(elementPos[v], k)
		} else {
			elementPos[v] = []int{k}
		}

		if v > maxPriority {
			maxPriority = v
		}
	}

	for maxPriority > 0 {
		if len(elementPos[maxPriority]) < 2 {
			maxPriority = searchMaximun(elementPos)
		}

		slices.Sort(elementPos[maxPriority])

		indexA := elementPos[maxPriority][0]
		indexB := elementPos[maxPriority][1]

		dePrioritize := maxPriority / 2
		if _, ok := elementPos[dePrioritize]; ok {
			elementPos[dePrioritize] = append(elementPos[dePrioritize], indexB)
		} else {
			elementPos[dePrioritize] = []int{indexB}
		}

		delete(priorityMap, indexA)
		priorityMap[indexB] = dePrioritize

		elementPos[maxPriority] = elementPos[maxPriority][2:]

		if len(queue[maxPriority]) >= 2 {
			continue
		}

		maxPriority = searchMaximun(elementPos)
	}

	positions := []int{}
	for _, k := range elementPos {
		positions = append(positions, k...)
	}

	slices.Sort(positions)
	result := []int32{}
	for _, k := range positions {
		result = append(result, priorityMap[k])
	}

	return result
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 16*1024*1024)

	stdout, err := os.Create(os.Getenv("OUTPUT_PATH"))
	checkError(err)

	defer stdout.Close()

	writer := bufio.NewWriterSize(stdout, 16*1024*1024)

	firstMultipleInput := strings.Split(strings.TrimSpace(readLine(reader)), " ")

	nTemp, err := strconv.ParseInt(firstMultipleInput[0], 10, 64)
	checkError(err)
	n := int32(nTemp)

	checkError(err)

	priorityStrings := strings.Split(strings.TrimSpace(readLine(reader)), " ")

	var priority []int32

	for i := 0; i < int(n); i++ {
		p, err := strconv.ParseInt(priorityStrings[i], 10, 64)
		checkError(err)
		item := int32(p)
		priority = append(priority, item)
	}

	result := processQueue(priority)

	printResult := []string{}
	for _, v := range result {
		printResult = append(printResult, strconv.Itoa(int(v)))
	}
	fmt.Fprintf(writer, "%s\n", strings.Join(printResult, " "))
	writer.Flush()
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
