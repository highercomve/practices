#!/bin/bash

set -e

output_file=${PWD}/$2 
cat $1 | OUTPUT_PATH=$output_file go run .
expected=`cat $3`
output=`cat $output_file`

if [ "$output" == "$expected" ]; then
  echo "Test Passed :)"
else 
  echo "expected: " >&2
  echo "$expected" >&2
  echo "output: " >&2
  echo "$output" >&2
  echo "Test Failed" >&2
  exit 1
fi