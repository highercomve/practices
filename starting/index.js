/**
  Given a function 

    char[] read4k();
  
  that continues to read next 4k chars from an internally maintained file, and returns a char array up to 4K in length.
  E.g. it can be called like:
  char[] buffer = read4k(buffer)
  
  buffer.length will be <= 4K, depending on how much content is left in the file

  
  implement a function 
  
    string readLine();
  
  A line is a sequence of characters followed by the '\n' character. 
  */


  let arrayOfChar = []

  function readLine() {
    if (arrayOfChar.length == 0) {
      arrayOfChar = read4k();
      if (arrayOfChar.length === 0) {
        return '' 
      }
    }
    
    let pointer = 0
    let char = arrayOfChar.shift()
    let line = ''
    
    while (char !== '\n') {
      line = line + char
      
      if (arrayOfChar.length == 0) {
        arrayOfChar = read4k();
        if (arrayOfChar.length == 0) {
          return line
        }
      }
      
      char = arrayOfChar.shift()
    }
    
    return line
  }
  
  ['h', 'e', '\n', 'm', 'i', 'n', 'g', ' ', 'i', 's', '\n', 's']
  
  readline()
  
  arrayOfChar.length = 0
  arrayOfChar = ['h', 'e', '\n', 'm']
  
  char ==> ['h']
  line = ''
  
  line = 'h'
  
  arrayOfChar.length === 3
  char ===> 'e'
  line === 'he'
  
  arrayOfChar.length === 2
  char ===> '\n'
  line === 'he'
  
  return 'he'
  
  
  readline()
  
  arrayOfChar.length = 0
  arrayOfChar = ['m']
  
  char ==> ['m']
  line = ''
  
  line = 'm'
  
  arrayOfChar.length === 0
  read4k()
  
  arrayOfChar === ['i', 'n', 'g', ' ']
  
  char ===> 'i'
  line === 'mi'
  
  arrayOfChar.length === 2
  char ===> 'n'
  line === 'min'
  
  arrayOfChar.length === 1
  char ===> 'g'
  line === 'ming'
  
  arrayOfChar.length === 0
  char ===> ' '
  line === 'ming '
  
  arrayOfChar.length === 0
  read4k()
  arrayOfChar =  ['i', 's', '\n', 's']
  
  char === 'i'
  line = 'ming i'
  arrayOfChar.length === 3
  
  char === 's'
  line = 'ming is'
  arrayOfChar.length === 2
  
  char === '\n'
  line = 'ming is\n'
  arrayOfChar.length === 1
  
  
  /**
  Given an array and a total, does the array have a contiguous subsequence that sums to the total?
  
  Ex: 
  [1, 5, 9, 7, 3]
  
  21 = 5 + 9 + 7  -> return true
  13 = 1 + 5 + 9 ... 7? -> return false 
  3 = 3           -> return true
  */
  
  
  function isSuminside(arr, target) {
    for (var i = 0; i < arr.length; i++) {
      let rest = target - arr[i] 
      if (rest === 0) {
        return true 
      }
      
      if (i === arr.length - 1) {
        return false
      }
      
      for (var j = i + 1; j < arr.length; j++) {
        rest = target - arr[j]
        if (rest === 0) {
          return true
        }
      }
    }
    return false
  }