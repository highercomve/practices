// Add any extra import statements you may need here


// Add any helper functions you may need here


function keyThere(groups, a) {
  let key = -1
  for (let i = 0; i <= a; i++) {
    if (groups[i] && groups[i][a]) {
      key = i
      break
    }
  }
  return key
}

function countRelationships(relations) {
  const groups = {}

  for (let i = 0; i < relations.length; i++) {
    for (let j = 0; j < relations.length; j++) {
      if (relations[i][j] === 0) { continue }

      const whereIsJ = keyThere(groups, j)
      let whereIsI = keyThere(groups, i)
      if (whereIsJ < 0) {
        if (whereIsI < 0 && !groups[i]) {
          groups[i] = {}
          whereIsI = i
        }
        groups[whereIsI][j] = true
      }

      if (whereIsJ > 0 && whereIsI !== whereIsJ) {
        delete groups[whereIsJ][j]
        groups[whereIsI][j] = true
      }

      for (let x = j + 1; x < relations.length; x++) {
        if (relations[j][x] === 0) { continue }

        const whereIsJ = keyThere(groups, j)
        if (keyThere(groups, x) < 0) {
          groups[whereIsJ][x] = true
        }
      }
    }
  }

  return Object.keys(groups).reduce((acc, k) => Object.keys(groups[k]).length > 0 ? acc + 1 : acc, 0)
}











// These are the tests we use to determine if the solution is correct.
// You can add your own at the bottom, but they are otherwise not editable!
var test_case_number = 1;

function check(expected, output) {
  const result = expected == output
  var rightTick = "\u2713";
  var wrongTick = "\u2717";
  if (result) {
    var out = rightTick + ' Test #' + test_case_number;
    console.log(out);
  }
  else {
    var out = '';
    out += wrongTick + ' Test #' + test_case_number + ': Expected ';
    out += expected;
    out += ' Your output: ';
    out += output;
    console.log(out);
  }
  test_case_number++;
}

var relations = [
  [1,1,0,0],
  [1,1,1,0],
  [0,1,1,0],
  [0,0,0,1]
];
var expected_1 = 2;
var output_1 = countRelationships(relations);
check(expected_1, output_1);

var relations = [
  [1,1,0,1],
  [1,1,1,0],
  [0,1,1,0],
  [1,0,0,1]
];
var expected_2 = 1;
var output_2 = countRelationships(relations);
check(expected_2, output_2);

var relations = [
  [1, 0, 1, 1, 0, 0],
  [0, 1, 0, 0, 0, 1],
  [1, 0, 1, 0, 1, 0],
  [1, 0, 0, 1, 0, 1],
  [0, 0, 1, 0, 1, 0],
  [0, 1, 0, 1, 0, 1],
];
var output_3 = countRelationships(relations);
var expected_3 = 1;
check(expected_3, output_3);

// Add your own test cases here
