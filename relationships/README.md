Relationships
==================

Count the numbers of groups of related users inside a matrix of relationships

  0 1 2 3
0 1 1 0 0 // Relations of 0 with all other users
1 1 1 1 0 
2 0 1 1 0
3 0 0 0 1

0-1 and 1 - 2 therefore 0, 1, 2 are a group of people that know each other
3 is alone

number of groups 2

Example two

    0  1  2  3  4  5
0  [1, 0, 1, 1, 0, 0]
1  [0, 1, 0, 0, 0, 1]
2  [1, 0, 1, 0, 1, 0]
3  [1, 0, 0, 1, 0, 1]
4  [0, 0, 1, 0, 1, 0]
5  [0, 1, 0, 1, 0, 1]

1 group