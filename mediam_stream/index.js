// Add any extra import statements you may need here


// Add any helper functions you may need here
const getLeftChildIndex = (i) => 2 * i + 1
const getRightChildIndex = (i) => 2 * i + 2
const getParentIndex = (i) => Math.floor((i - 1) / 2)

const hasLeftChild = (heap = [], i) => getLeftChildIndex(i) < heap.length
const hasRightChild = (heap = [], i) => getRightChildIndex(i) < heap.length
const hasParent = (i) => getParentIndex(i) >= 0

const defaultSort = (a, b) => a > b

const swap = (h, indexa, indexb) => {
  const a = h[indexa]
  h[indexa] = h[indexb]
  h[indexb] = a
}

const heapify = (h, i, sorting = defaultSort) => {
  let sorteableIndex = i; // Initialize sorteableIndex as root
  const l = getLeftChildIndex(i)
  const r = getRightChildIndex(i)
  
  while(hasLeftChild(h, i) || hasRightChild(h, i)) {
    if (hasLeftChild(h, i) && sorting(h[l], h[sorteableIndex])) {
      sorteableIndex = l;
    }
  
    if (hasRightChild(h, i) && sorting(h[r], h[sorteableIndex])) {
      sorteableIndex = r;
    }
    
    if (sorteableIndex === i) {
      break
    }
     
    swap(h, i, sorteableIndex);
    i = sorteableIndex;
  }
}

const heapifyUp = (h, sorting = defaultSort) => {
  let index = h.length - 1 < 0 ? 0 : h.length - 1
  while(hasParent(index) && sorting(h[index], h[getParentIndex(index)])) {
    swap(h, getParentIndex(index), index)
    index = getParentIndex(index)
  }
}

const Heap = (sorting = defaultSort) => {
  let h = []

  const add = (...a) => {
    a.forEach(toa => {
      h.push(toa)
      heapifyUp(h, sorting)
    })
  }

  const poll = () => {
    const polled = h.shift()
    heapify(h, 0, sorting)
    return polled
  }

  const sorted = (k = h.length) => {
    const r = []
    for (var i = 0; i < k; i++) {
      r.push(poll())
    }
    return r
  }

  const size = () => h.length
  const all = () => h
  const peek = () => h[0]
  const resize = (n) => h = sorted(n)

  return {
    add,
    size,
    poll,
    resize,
    all,
    peek,
    sorted,
  }
}

const balanceHeaps = (heapA, heapB) => {
  const biggerHeap = heapA.size() > heapB.size() ? heapA : heapB
  const smallerHeap = heapB.size() < heapA.size() ? heapB : heapA
  
  if (biggerHeap.size() - smallerHeap.size() >= 2) {
    smallerHeap.add(biggerHeap.poll())
  }
}

const getMedian = (heapA, heapB) => {
  const biggerHeap = heapA.size() > heapB.size() ? heapA : heapB
  const smallerHeap = heapB.size() < heapA.size() ? heapB : heapA
  
  if (biggerHeap.size() === smallerHeap.size()) {
    return Math.floor((biggerHeap.peek() + smallerHeap.peek()) / 2)
  }
  
  return biggerHeap.peek()
}


function findMedian(arr) {
  // Write your code here
  const lowerHeap = Heap()
  const higherHeap = Heap((a, b) => a < b)
  
  return arr.reduce((acc, value) => {
    if (higherHeap.peek() && value > higherHeap.peek()) {
      higherHeap.add(value)
    } else {
      lowerHeap.add(value)
    }

    balanceHeaps(higherHeap, lowerHeap)
    
    acc.push(getMedian(lowerHeap, higherHeap))
    return acc
  }, [])
}











// These are the tests we use to determine if the solution is correct.
// You can add your own at the bottom, but they are otherwise not editable!
function printintegerArray(array) {
  var size = array.length;
  var res = '';
  res += '[';
  var i = 0;
  for (i = 0; i < size; i++) {
    if (i !== 0) {
    	res += ', ';
    }
    res += array[i];
  }
  res += ']';
  return res;
}

var test_case_number = 1;

function check(expected, output) {
  var expected_size = expected.length;
  var output_size = output.length;
  var result = true;
  if (expected_size != output_size) {
    result = false;
  }
  for (var i = 0; i < Math.min(expected_size, output_size); i++) {
    result &= (output[i] == expected[i]);
  }
  var rightTick = "\u2713";
	var wrongTick = "\u2717";
  if (result) {
  	var out = rightTick + ' Test #' + test_case_number;
  	console.log(out);
  }
  else {
  	var out = '';
  	out += wrongTick + ' Test #' + test_case_number + ': Expected ';
  	out += printintegerArray(expected);
  	out += ' Your output: ';
  	out += printintegerArray(output);
  	console.log(out);
  }
  test_case_number++;
}

var arr_1 = [5, 15, 1, 3];
var expected_1 = [5, 10, 5, 4];
var output_1 = findMedian(arr_1);
check(expected_1, output_1);

var arr_2 = [2, 4, 7, 1, 5, 3];
var expected_2 = [2, 3, 4, 3, 4, 3];
var output_2 = findMedian(arr_2);
check(expected_2, output_2);

// Add your own test cases here
var arr_2 = [2, 4, 7, 1, 5, 3];
var expected_2 = [2, 3, 4, 3, 4, 3];
var output_2 = findMedian(arr_2);
check(expected_2, output_2);