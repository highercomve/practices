// Add any extra import statements you may need here

// Add any helper functions you may need here
function Graphs() {
  this.nodes = new Map()  
}

Graphs.prototype.add = function (node) {
  this.nodes.set(node.id, node)
} 

Graphs.prototype.addEdge = function (source, destination) {
  if (!this.nodes.has(source)) {
    this.nodes.set(source, new Node(source))
  }
  if (!this.nodes.has(destination)) {
    this.nodes.set(destination, new Node(destination))
  }
  
  const a = this.nodes.get(source)
  const b = this.nodes.get(destination)
  b.parent = a
  a.add(b)
}


Graphs.prototype.getNode = function (id) {
  return this.nodes.get(id)
}

function Node(id, parent) {
  this.id = id
  this.parent = parent
  this.adj = new Map()
}

Node.prototype.add = function (node) {
  this.adj.set(node.id, node)
}

const permutate = (arr, parent, start, end, graph, memo = {}) => {
  if (start >= end || end < 0 || end <= start) {
    return
  }

  const arrID = arr.join('')
  const subArr = arr.slice(0, start)
    .concat(arr.slice(start, end + 1).reverse())
    .concat(arr.slice(end + 1, arr.length))

  const id = subArr.join('')
  graph.addEdge(parent.id, id)
  if (memo[arrID+id]) {
    return graph.nodes.get(memo[arrID+id])
  } else {
    memo[arrID+id] = id
  }


  const initialI = start
  const endI = end
  for (let i = initialI; i < endI; i++) {
    permutate(arr, parent, i + 1, end, graph, memo)
    permutate(arr, parent, 0, end - i - 1, graph, memo)
    permutate(arr, parent, i, i + 1, graph, memo)
  }

  return permutate(subArr, graph.nodes.get(id), 0, subArr.length, graph, memo)
}

function minOperations(arr) {
  // Write your code here
  const g = new Graphs()
  const id = arr.join('')
  const base = new Node(id)
  g.nodes.set(id, base)
  permutate(arr, base, 0, arr.length, g, {})

  const visited = new Map()
  const sortedId = arr.sort((a, b) => a - b).join('')
  const queue = [base]
  
  const dist = {[id]: 0}
  while (queue.length > 0) {
    const node = queue.shift()
    for ([k, n] of node.adj) {
      if (visited.has(n.id)) {
        continue
      }
  
      visited.set(n.id, true)
      dist[n.id] = dist[node.id] + 1
      queue.push(n)

      if (n.id === sortedId) {
        return dist[n.id]
      }
    }
  }
  
  return dist[sortedId]
}











// These are the tests we use to determine if the solution is correct.
// You can add your own at the bottom, but they are otherwise not editable!
function printInteger(n) {
  var out = '[' + n + ']';
  return out;
}

var test_case_number = 1;

function check(expected, output) {
  var result = (expected == output);
  var rightTick = "\u2713";
  var wrongTick = "\u2717";
  if (result) {
    var out = rightTick + ' Test #' + test_case_number;
    console.log(out);
  }
  else {
    var out = '';
    out += wrongTick + ' Test #' + test_case_number + ': Expected ';
    out += printInteger(expected);
    out += ' Your output: ';
    out += printInteger(output);
    console.log(out);
  }
  test_case_number++;
}

var n_1 = 5;
var arr_1 = [1, 2, 5, 4, 3];
var expected_1 = 1;
var output_1 = minOperations(arr_1);
check(expected_1, output_1);

var n_2 = 3;
var arr_2 = [3, 1, 2];
var expected_2 = 2;
var output_2 = minOperations(arr_2);
check(expected_2, output_2);

var expected_3 = 3;
var output_3 = minOperations([5, 3, 1, 2, 4]);
check(expected_3, output_3);