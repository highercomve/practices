function Router() {
  this.paths = new Map()
}

Router.prototype.has = function(to) {
  return this.paths.has(to.id) 
}

Router.prototype.add = function (from, to, weight, extra = {}) {
  this.paths.set(to.id, { to, from, weight, ...extra }) 
}

Router.prototype.get = function (to) {
  return this.paths.get(to.id) 
}

Router.prototype.getPath = function (m) {
  let path = []
  if (!m.from) {
    return path
  }
  path.push(m.to.id)
  while (m) {
    path.push(m.from.id)
    m = this.get(m.from)
  }

  return path.reverse()
}
function Graphs() {
  this.nodes = new Map()
}

Graphs.prototype.add = function (node) {
  this.nodes.set(node.id, node)
}

Graphs.prototype.addEdge = function (source, destination, weight = 1) {
  if (!this.nodes.has(source)) {
    this.nodes.set(source, new Node(source))
  }
  if (!this.nodes.has(destination)) {
    this.nodes.set(destination, new Node(destination))
  }
  
  const a = this.nodes.get(source)
  const b = this.nodes.get(destination)
  a.add(b, weight)
  b.add(a, weight)
}

Graphs.prototype.getNode = function (id) {
  return this.nodes.get(id)
}

function Node(id) {
  this.id = id
  this.adj = new Map()
}

Node.prototype.add = function (node, weight = 1) {
  this.adj.set(node.id, { node, weight })
}

function MinimalPath(graph) {
  this.graph = graph
  this.routes = new Map()
}

MinimalPath.prototype.routerFrom = function (from) {
  if (!this.routes.has(from)) {
    this.routes.set(from, new Router())
  }
  return this.routes.get(from)
}

MinimalPath.prototype.calculate = function (fromId, toId) {
  let from = this.graph.getNode(fromId)
  const to = this.graph.getNode(toId)
  if (!from || !to) {
    return undefined
  }
  const routes = this.routerFrom(fromId)
  if ((routes.get(to) || {}).final) {
    return routes.get(to)
  }
  let visited = new Map()
  while (from) {
    let smallest
    if (!visited.has(from.id)) {
      visited.set(from.id, true)
    }
    for (var [key, adj] of from.adj) {
      if (adj.node.id === fromId) {
        continue
      }
      if (visited.has(adj.node.id)) {
        continue
      }
      let minimal = routes.get(adj.node)
      if (!minimal) {
        routes.add(from, adj.node, (routes.get(from) || { weight: 0 }).weight + adj.weight)
        minimal = routes.get(adj.node)
      }

      if (adj.node.id === minimal.from.id) {
        continue
      }

      const prevRoute = (routes.get(minimal.from) || { weight: 0 }).weight + minimal.weight
      const newRoute = (routes.get(from) || { weight: 0 }).weight + adj.weight
      if (prevRoute > newRoute) {
        routes.add(from, adj.node, newRoute)
        minimal = routes.get(adj.node)
      }

      if (!smallest || smallest.weight > minimal.weight) {
        smallest = minimal
      }
    }

    const minimal = routes.get(this.graph.getNode(toId))
    if (!smallest || minimal && minimal.weight < smallest.weight) {
      routes.add(minimal.from, minimal.to, minimal.weight, { final: true })
      return minimal
    }
    
    from = smallest.to
  }
}



// These are the tests we use to determine if the solution is correct.
// You can add your own at the bottom, but they are otherwise not editable!
function printInteger(n) {
  var out = '[' + n + ']';
  return out;
}

var test_case_number = 1;

function check(expected, output) {
  var result = (expected == output);
  var rightTick = "\u2713";
  var wrongTick = "\u2717";
  if (result) {
    var out = rightTick + ' Test #' + test_case_number;
    console.log(out);
  }
  else {
    var out = '';
    out += wrongTick + ' Test #' + test_case_number + ': Expected ';
    out += printInteger(expected);
    out += ' Your output: ';
    out += printInteger(output);
    console.log(out);
  }
  test_case_number++;
}

function getResult(result) {
  return `from: ${result.from.id} weight: ${result.weight}`
}

function getPath(r, m) {
  let path = []
  if (!m.from) {
    return path
  }
  path.push(m.to.id)
  while (m) {
    path.push(m.from.id)
    m = r.get(m.from)
  }

  return path.reverse()
}

var g = new Graphs();
g.addEdge('a', 'b', 8)
g.addEdge('a', 'c', 2)
g.addEdge('a', 'd', 5)
g.addEdge('b', 'd', 2)
g.addEdge('b', 'f', 13)
g.addEdge('c', 'd', 2)
g.addEdge('c', 'e', 5)
g.addEdge('d', 'b', 2)
g.addEdge('d', 'e', 1)
g.addEdge('d', 'g', 3)
g.addEdge('d', 'f', 6)
g.addEdge('e', 'g', 1)
g.addEdge('g', 'f', 2)
g.addEdge('g', 'h', 6)
g.addEdge('f', 'h', 3)

const d = new MinimalPath(g)
var output = d.calculate('a', 'b');
expected = `from: d weight: 6`
check(expected, getResult(output));
check(['a', 'c', 'd', 'b'].join(', '), d.routerFrom('a').getPath(output).join(', '));

var output = d.calculate('a', 'd');
expected = `from: c weight: 4`
check(expected, getResult(output));
check(['a', 'c', 'd'].join(', '), d.routerFrom('a').getPath(output).join(', '));

var output = d.calculate('a', 'h');
expected = `from: f weight: 11`
check(expected, getResult(output));
check(['a', 'c', 'd', 'e', 'g', 'f', 'h'].join(', '), d.routerFrom('a').getPath(output).join(', '));

var output = d.calculate('a', 'f');
expected = `from: g weight: 8`
check(expected, getResult(output));
check(['a', 'c', 'd', 'e', 'g', 'f'].join(', '), d.routerFrom('a').getPath(output).join(', '));

var output = d.calculate('a', 'k');
check(undefined, output);
