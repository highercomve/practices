package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

/*
 * Complete the 'maximumToys' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts following parameters:
 *  1. INTEGER_ARRAY prices
 *  2. INTEGER k
 */

func maximumToys(p []int32, k int32) int32 {
	prices := mergeSort(p)
	lenPrices := int32(len(prices))
	maxToys := int32(0)
	sum := int32(0)
	nextSum := int32(0)
	sum = prices[0]
	count := int32(1)
	for j := int32(1); j < lenPrices; j++ {
		nextSum = sum + prices[j]
		if nextSum > k {
			break
		}
		count = count + 1
		sum = nextSum
	}
	if count > maxToys {
		maxToys = count
	}

	return maxToys
}

func mergeSort(arr []int32) []int32 {
	// No need to sort the array if the array only has one element or empty
	if len(arr) <= 1 {
		return arr
	}

	// In order to divide the array in half, we need to figure out the middle
	middle := len(arr) / 2

	// Divide the arrays in a left and right part
	left := mergeSort(arr[:middle])
	right := mergeSort(arr[middle:])

	// Combine the left and the right
	return merge(left, right)
}

func merge(left []int32, right []int32) []int32 {
	result := make([]int32, 0, len(left)+len(right))

	// Concatente values to the result array
	for len(left) > 0 || len(right) > 0 {
		if len(left) == 0 {
			return append(result, right...)
		}

		if len(right) == 0 {
			return append(result, left...)
		}

		if left[0] <= right[0] {
			result = append(result, left[0])
			left = left[1:]
		} else {
			result = append(result, right[0])
			right = right[1:]
		}
	}

	return result
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 16*1024*1024)

	stdout, err := os.Create(os.Getenv("OUTPUT_PATH"))
	checkError(err)

	defer stdout.Close()

	writer := bufio.NewWriterSize(stdout, 16*1024*1024)

	firstMultipleInput := strings.Split(strings.TrimSpace(readLine(reader)), " ")

	nTemp, err := strconv.ParseInt(firstMultipleInput[0], 10, 64)
	checkError(err)
	n := int32(nTemp)

	kTemp, err := strconv.ParseInt(firstMultipleInput[1], 10, 64)
	checkError(err)
	k := int32(kTemp)

	pricesTemp := strings.Split(strings.TrimSpace(readLine(reader)), " ")

	var prices []int32

	for i := 0; i < int(n); i++ {
		pricesItemTemp, err := strconv.ParseInt(pricesTemp[i], 10, 64)
		checkError(err)
		pricesItem := int32(pricesItemTemp)
		prices = append(prices, pricesItem)
	}

	result := maximumToys(prices, k)

	fmt.Fprintf(writer, "%d\n", result)

	writer.Flush()
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
