'use strict';

const fs = require('fs');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', function (inputStdin) {
	inputString += inputStdin;
});

process.stdin.on('end', function () {
	inputString = inputString.split('\n');

	main();
});

function readLine() {
	return inputString[currentLine++];
}

/*
 * Complete the 'activityNotifications' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts following parameters:
 *  1. INTEGER_ARRAY expenditure
 *  2. INTEGER d
 */

function activityNotifications(expenditure = [], d) {
	let count = 0
	let median = 0
	let counts = []

	for (let i = 0; i <= 200; i++) {
		counts[i] = 0
	}

	for (let i = 0; i < expenditure.length; i++) {
		const val = expenditure[i]
		if (i < d) {
			counts[val] += 1
			continue
		}

		median = getMedian(counts, d)
		if (val >= (median * 2)) {
			count++
		}

		counts[expenditure[i-d]] -= 1
		counts[val] += 1
	}

	return count
}

function getMedianForIdx(counts = [], idx) {
	let sum = 0
	for (let i = 0; i < counts.length; i++) {
		sum = sum + counts[i]
		if (sum >= idx) {
			return i
		}
	}
	return -1
}

function getMedian(counts = [], d = 0) {
	let median = getMedianForIdx(counts, Math.floor(d/2) + (d % 2))

	if (d % 2 == 0) {
		median = (median + getMedianForIdx(counts, Math.floor(d/2) + 1)) / 2
	}

	return median
}

function main() {
	const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

	const firstMultipleInput = readLine().replace(/\s+$/g, '').split(' ');

	const n = parseInt(firstMultipleInput[0], 10);

	const d = parseInt(firstMultipleInput[1], 10);

	const expenditure = readLine().replace(/\s+$/g, '').split(' ').map(expenditureTemp => parseInt(expenditureTemp, 10));

	const result = activityNotifications(expenditure, d);

	ws.write(result + '\n');

	ws.end();
}
