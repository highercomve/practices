package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

/*
 * Complete the 'activityNotifications' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts following parameters:
 *  1. INTEGER_ARRAY expenditure
 *  2. INTEGER d
 */

func activityNotifications(expenditure []int32, d int32) int32 {
	count := int32(0)
	median := float32(0)
	counts := make([]int32, 200)

	if int32(len(expenditure)) <= d {
		return count
	}

	for i := 0; i < len(expenditure); i++ {
		val := expenditure[i]
		if i < int(d) {
			counts[val] += 1
			continue
		}

		median = getMedian(counts, d)
		if float32(val) >= (median * 2) {
			count++
		}

		counts[expenditure[i-int(d)]] -= 1
		counts[val] += 1
		// fmt.Printf("i: %v -- median: %v -- value: %v -- count: %v\n", i, median, expenditure[i], count)
	}

	return count
}

func getMedianIdx(counts []int32, idx int32) float32 {
	sum := int32(0)
	for i := 0; i < len(counts); i++ {
		sum = sum + counts[i]
		if sum >= idx {
			return float32(i)
		}
	}

	return float32(-1)
}

func getMedian(counts []int32, d int32) float32 {
	m := getMedianIdx(counts, d/2+(d%2))

	if d%2 == 0 {
		m = (m + getMedianIdx(counts, d/2+1)) / 2
	}

	return m
}

func PrintCounts(counts []int32) {
	mapper := map[int]int32{}
	for i, v := range counts {
		if v > 0 {
			mapper[i] = v
		}
	}

	fmt.Printf("%+v\n", mapper)
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 16*1024*1024)

	stdout, err := os.Create(os.Getenv("OUTPUT_PATH"))
	checkError(err)

	defer stdout.Close()

	writer := bufio.NewWriterSize(stdout, 16*1024*1024)

	firstMultipleInput := strings.Split(strings.TrimSpace(readLine(reader)), " ")

	nTemp, err := strconv.ParseInt(firstMultipleInput[0], 10, 64)
	checkError(err)
	n := int32(nTemp)

	dTemp, err := strconv.ParseInt(firstMultipleInput[1], 10, 64)
	checkError(err)
	d := int32(dTemp)

	expenditureTemp := strings.Split(strings.TrimSpace(readLine(reader)), " ")

	var expenditure []int32

	for i := 0; i < int(n); i++ {
		expenditureItemTemp, err := strconv.ParseInt(expenditureTemp[i], 10, 64)
		checkError(err)
		expenditureItem := int32(expenditureItemTemp)
		expenditure = append(expenditure, expenditureItem)
	}

	result := activityNotifications(expenditure, d)

	fmt.Fprintf(writer, "%d\n", result)

	writer.Flush()
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

// func ActivityNotificationsPerf(expenditure []int32, d int32) int32 {
// 	count := int32(0)
// 	counts := make([]int32, 200)

// 	if int32(len(expenditure)) <= d {
// 		return count
// 	}

// 	sortedMedian := mergeSort(expenditure[0:d])
// 	for i := int32(d); i < int32(len(expenditure)); i++ {
// 		median := getMedian(sortedMedian)
// 		if float32(expenditure[i]) >= (median * 2) {
// 			count++
// 		}

// 		sortedMedian, _ = appendMedian(sortedMedian, expenditure[i], expenditure[i-d], false)
// 	}

// 	return count
// }

// func ActivityNotifications(expenditure []int32, d int32) int32 {
// 	count := int32(0)

// 	if int32(len(expenditure)) <= d {
// 		return count
// 	}

// 	for i := int32(d); i < int32(len(expenditure)); i++ {
// 		median := getMedian(mergeSort(expenditure[i-d : i]))

// 		if float32(expenditure[i]) >= (median * 2) {
// 			count++
// 		}
// 		fmt.Printf("i: %v -- median: %v -- value: %v -- count: %v\n", i, median, expenditure[i], count)
// 	}

// 	return count
// }

// func appendMedian(sorted []int32, in int32, out int32, debug bool) ([]int32, int) {
// 	newSorted := sorted
// 	outFound := -1
// 	for i := 0; i < len(sorted); i++ {
// 		if sorted[i] == out {
// 			newSorted = append([]int32{}, sorted[0:i]...)
// 			if i+1 < len(sorted) {
// 				newSorted = append(newSorted, sorted[i+1:]...)
// 			}
// 			outFound = i
// 			break
// 		}
// 	}

// 	if in <= newSorted[0] {

// 		newSorted = append([]int32{in}, newSorted...)

// 	} else if in >= newSorted[len(newSorted)-1] {

// 		newSorted = append(newSorted, in)

// 	} else {

// 		for i := 0; i < len(newSorted)-2; i++ {
// 			if in >= newSorted[i] && in <= newSorted[i+1] {
// 				b := append(newSorted[0:i+1], in)
// 				if i+1 < len(sorted) {
// 					b = append(b, newSorted[i+1:]...)
// 				}

// 				newSorted = b
// 				break
// 			}
// 		}

// 	}

// 	return newSorted, outFound
// }

// func getMedian(lastN []int32) float32 {
// 	middle := len(lastN) / 2
// 	if len(lastN)%2 == 0 {
// 		return (float32(lastN[middle]) + float32(lastN[middle-1])) / 2
// 	}

// 	return float32(lastN[middle])

// }

// func mergeSort(arr []int32) []int32 {
// 	// No need to sort the array if the array only has one element or empty
// 	if len(arr) <= 1 {
// 		return arr
// 	}

// 	// In order to divide the array in half, we need to figure out the middle
// 	middle := len(arr) / 2

// 	// Divide the arrays in a left and right part
// 	left := mergeSort(arr[:middle])
// 	right := mergeSort(arr[middle:])

// 	// Combine the left and the right
// 	return merge(left, right)
// }

// func merge(left []int32, right []int32) []int32 {
// 	result := make([]int32, 0, len(left)+len(right))

// 	// Concatente values to the result array
// 	for len(left) > 0 || len(right) > 0 {
// 		if len(left) == 0 {
// 			return append(result, right...)
// 		}

// 		if len(right) == 0 {
// 			return append(result, left...)
// 		}

// 		if left[0] <= right[0] {
// 			result = append(result, left[0])
// 			left = left[1:]
// 		} else {
// 			result = append(result, right[0])
// 			right = right[1:]
// 		}
// 	}

// 	return result
// }
