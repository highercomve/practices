// Add any extra import statements you may need here


// Add any helper functions you may need here
const getMedian = (arr) => {
  return Math.floor(Math.abs(arr[0] + arr[arr.length - 1]) / 2)
}

function minOverallAwkwardness(arr) {
  // Write your code here
  arr = arr.sort((a, b) => a - b);
  let diff = Math.max(arr[1] - arr[0], arr[arr.length - 1] - arr[arr.length - 2]);
  
  for(let i = 2; i < arr.length; i++){
    diff = Math.max(diff, arr[i] - arr[i - 2]);
  }
  
  return diff;

  // if (arr.length <= 2) {
  //   return Math.abs(arr[0] - arr[1])
  // }

  // const sortedArray = arr.sort((a, b) => a - b)
  // const median = getMedian(sortedArray)
  // let medianIndex = 0
  // let maxW = 0
  // for (var i = 0; i < sortedArray.length; i++) {
  //   if (sortedArray[i] > median) {
  //     medianIndex = i - 1 < 0 ? 0 : i - 1
  //     break
  //   }
  // }

  
  // return sortedArray.slice(0, medianIndex)
  //   .concat(sortedArray.slice(medianIndex + 1, sortedArray.length))
  //   .concat([sortedArray[medianIndex]])
  //   .reduce((max, v, index, a) => {
  //     const next = i === (a.length - 1) ? 0 : index + 1
  //     const w = Math.abs(v - a[next])
  //     return w > max ? w : max
  //   }, 0)
}











// These are the tests we use to determine if the solution is correct.
// You can add your own at the bottom, but they are otherwise not editable!
function printInteger(n) {
  var out = '[' + n + ']';
  return out;
}

var test_case_number = 1;

function check(expected, output) {
  var result = (expected == output);
  var rightTick = "\u2713";
  var wrongTick = "\u2717";
  if (result) {
    var out = rightTick + ' Test #' + test_case_number;
    console.log(out);
  }
  else {
    var out = '';
    out += wrongTick + ' Test #' + test_case_number + ': Expected ';
    out += printInteger(expected);
    out += ' Your output: ';
    out += printInteger(output);
    console.log(out);
  }
  test_case_number++;
}

var arr_1 = [5, 10, 6, 8];
var expected_1 = 4;
var output_1 = minOverallAwkwardness(arr_1);
check(expected_1, output_1);

var arr_2 = [1, 2, 5, 3, 7];
var expected_2 = 4;
var output_2 = minOverallAwkwardness(arr_2);
check(expected_2, output_2);

// Add your own test cases here
var arr_2 = [64, 1, 10, 30, 12, 60, 62, 6, 5];
var expected_2 = 48;
var output_2 = minOverallAwkwardness(arr_2);
check(expected_2, output_2);