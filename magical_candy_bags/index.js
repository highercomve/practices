// Add any extra import statements you may need here
const getLeftChildIndex = (i) => 2 * i + 1
const getRightChildIndex = (i) => 2 * i + 2
const getParentIndex = (i) => Math.floor((i - 1) / 2)

const hasLeftChild = (heap = [], i) => getLeftChildIndex(i) < heap.length
const hasRightChild = (heap = [], i) => getRightChildIndex(i) < heap.length
const hasParent = (i) => getParentIndex(i) >= 0

const getLeftChild = (heap = [], i) => heap[getLeftChildIndex(i)]
const getRightChild = (heap = [], i) => heap[getRightChildIndex(i)]
const getParent = (heap = [], i) => heap[getParentIndex(i)]

const defaultSort = (a, b) => a > b

const swap = (h, indexa, indexb) => {
  const a = h[indexa]
  h[indexa] = h[indexb]
  h[indexb] = a
}

const heapify = (h, i, sorting = defaultSort) => {
  let sorteableIndex = i; // Initialize sorteableIndex as root
  const l = getLeftChildIndex(i)
  const r = getRightChildIndex(i)
  
  while(hasLeftChild(h, i) || hasRightChild(h, i)) {
    if (hasLeftChild(h, i) && sorting(h[l], h[sorteableIndex])) {
      sorteableIndex = l;
    }
  
    if (hasRightChild(h, i) && sorting(h[r], h[sorteableIndex])) {
      sorteableIndex = r;
    }
    
    if (sorteableIndex === i) {
      break
    }
     
    swap(h, i, sorteableIndex);
    i = sorteableIndex;
  }
}

const heapifyUp = (h, sorting = defaultSort) => {
  let index = h.length - 1 < 0 ? 0 : h.length - 1
  while(hasParent(index) && sorting(h[index], h[getParentIndex(index)])) {
    swap(h, getParentIndex(index), index)
    index = getParentIndex(index)
  }
}

const Heap = (sorting = defaultSort) => {
  let h = []

  const add = (...a) => {
    a.forEach(toa => {
      h.push(toa)
      heapifyUp(h, sorting)
    })
  }

  const poll = () => {
    const polled = h.shift()
    heapify(h, 0, sorting)
    return polled
  }

  return {
    add,
    poll,
  }
}

// Add any helper functions you may need here
function maxCandies(arr, k) {
  // Write your code here
  const heap = Heap()
  heap.add(...arr)
  let result = 0

  for (var i = 0; i < k; i++) {
    const max = heap.poll()
    result += max
    heap.add(Math.floor(max / 2))
  }

  return result
}











// These are the tests we use to determine if the solution is correct.
// You can add your own at the bottom, but they are otherwise not editable!
function printInteger(n) {
  var out = '[' + n + ']';
  return out;
}

var test_case_number = 1;

function check(expected, output) {
  var result = (expected == output);
  var rightTick = "\u2713";
	var wrongTick = "\u2717";
  if (result) {
  	var out = rightTick + ' Test #' + test_case_number;
  	console.log(out);
  }
  else {
  	var out = '';
  	out += wrongTick + ' Test #' + test_case_number + ': Expected ';
  	out += printInteger(expected);
  	out += ' Your output: ';
  	out += printInteger(output);
  	console.log(out);
  }
  test_case_number++;
}

var n_1 = 5, k_1 = 3;
var arr_1 = [2, 1, 7, 4, 2];
var expected_1 = 14;
var output_1 = maxCandies(arr_1, k_1);
check(expected_1, output_1);

var n_2 = 9, k_2 = 3;
var arr_2 = [19, 78, 76, 72, 48, 8, 24, 74, 29];
var expected_2 = 228;
var output_2 = maxCandies(arr_2, k_2);
check(expected_2, output_2); 

// Add your own test cases here
var n_2 = 9, k_2 = 5;
var arr_2 = [19, 78, 76, 72, 48, 8, 24, 74, 29];
var expected_2 = 348;
var output_2 = maxCandies(arr_2, k_2);
check(expected_2, output_2); 