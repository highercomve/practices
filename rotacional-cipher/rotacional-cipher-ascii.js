function LetterRotate (code, rotationFactor) {
  let newCode = code
  switch (true) {
    // Is alpha uppercase
    case code >= 65 && code <= 90:
      newCode = code + (rotationFactor % 26)
      return newCode > 90 ? newCode - 26 : newCode
    // Is alpha lowercase
    case code >= 97 && code <= 122:
      newCode = code + (rotationFactor % 26)
      return newCode > 122 ? newCode - 26 : newCode
    // Is number
    case code >= 48 && code <= 57:
      newCode = code + (rotationFactor % 10)
      return newCode > 57 ? newCode - 10 : newCode
    // Everything else
    default:
      return code
  }
}

function rotationalCipher(input, rotationFactor) {
  let result = ""
  for (var i = 0; i < input.length; i++) {
    const code = input.charCodeAt(i)
    const newCode = LetterRotate(code, rotationFactor)
    result += String.fromCharCode(newCode)
  }
  
  return result
}

var stdin = process.openStdin();

var data = "";

stdin.on('data', function(chunk) {
  data += chunk;
});

stdin.on('end', function() {
  for (var i; i < 100000; i++) {
    rotationalCipher(data, process.argv[2] || 100000)
  }

  const used = process.memoryUsage()
  for (let key in used) {
    console.log(`${key} ${Math.round(used[key] / 1024 / 1024 * 100) / 100} MB`)
  }
});

