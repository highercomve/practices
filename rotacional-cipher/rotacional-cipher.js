// Add any extra import statements you may need here


// Add any helper functions you may need here
const alpha = "abcdefghijklmnopqrstuvwxyz"
const numeric = "1234567890"
const mapToIndexAndValues = (mapped, v, index) => {
  mapped['indexOf'][v] = index
  mapped['valueOf'][index] = v
  return mapped
}
const mappedAlpha = alpha.split("").reduce(mapToIndexAndValues, { indexOf: {}, valueOf: {} })
const mappedNumeric = numeric.split("").reduce(mapToIndexAndValues, { indexOf: {}, valueOf: {} })

function rotationalCipher(input, rotationFactor) {
  let result = ""
  for (var i = 0; i < input.length; i++) {
    const letter = input[i]
    const lowercased = letter.toLowerCase()
    const alphaPos = mappedAlpha['indexOf'][lowercased]
    const numberPos = mappedNumeric['indexOf'][lowercased]

    if (alphaPos !== undefined && alphaPos >= 0) {
      result += letter === letter.toUpperCase()
        ? mappedAlpha['valueOf'][(alphaPos + rotationFactor) % alpha.length].toUpperCase()
        : mappedAlpha['valueOf'][(alphaPos + rotationFactor) % alpha.length]
      continue
    }
    
    if (numberPos !== undefined && numberPos >= 0) {
      result += mappedNumeric['valueOf'][(numberPos + rotationFactor) % numeric.length]
      continue
    }
    
    result += letter
  }
  
  return result
}

var stdin = process.openStdin();

var data = "";

stdin.on('data', function(chunk) {
  data += chunk;
});

stdin.on('end', function() {
  for (var i; i < 100000; i++) {
    rotationalCipher(data, process.argv[2] || 100000)
  }

  const used = process.memoryUsage()
  for (let key in used) {
    console.log(`${key} ${Math.round(used[key] / 1024 / 1024 * 100) / 100} MB`)
  }
});


