/**
 * @param {number[]} nums
 * @param {number} val
 * @return {number}
 */
var removeElement = function (nums, val) {
	let k = 0
	for (let i = 0; i < nums.length; i++) {
		if (nums[i] !== val) {
			continue
		}

		k++
		for (let j = i + 1; j < nums.length; j++) {
			if (nums[j] !== val) {
				nums[i] = nums[j]
				nums[j] = val
				break
			}
		}
	}

	for (let i = 1; i <= k; i ++) {
		nums.pop()
	}
	
	return k
};

var test_case_number = 1;
function equal(arr1, arr2) {
	if (arr1.length !== arr2.length) { return false }
	for (let i = 1; i < arr1.length; i++) {
		if (arr1[i] !== arr2[i]) {
			return false
		}
	}

	return true
}
function check(expected, output) {
	var result = equal(expected, output);
	var rightTick = "\u2713";
	var wrongTick = "\u2717";
	if (result) {
		var out = rightTick + ' Test #' + test_case_number;
		console.log(out);
	}
	else {
		var out = '';
		out += wrongTick + ' Test #' + test_case_number + ': Expected ';
		out += `${expected}`;
		out += ' Your output: ';
		out += `${output}`;
		console.log(out);
	}
	test_case_number++;
}

var input1 = [1, 2, 3, 0, 0, 0]
var expected = [1, 2, 3, 0, 0, 0]
var k = 0
var output = removeElement(input1, 6)
check(expected, input1)
check(k, output)

var input1 = [1, 2, 3, 6, 6, 7, 9, 0, 0]
var expected = [1, 2, 3, 7, 9, 0, 0, 6, 6]
var k = 2
var output = removeElement(input1, 6)
check(expected, input1)
check(k, output)

var input1 = [1, 2, 3, 2, 6, 7, 9, 2, 0]
var expected = [1, 3, 6, 7, 9, 0, 2, 2, 2]
var k = 3
var output = removeElement(input1, 2)
check(expected, input1)
check(k, output)

var input1 = [0, 1, 2, 2, 3, 0, 4, 2]
var expected = [0, 1, 4, 0, 3]
var k = 3
var output = removeElement(input1, 2)
check(expected, input1)
check(k, output)
