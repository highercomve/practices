/**
 * @param {number[]} nums1
 * @param {number} m
 * @param {number[]} nums2
 * @param {number} n
 * @return {void} Do not return anything, modify nums1 in-place instead.
 */
var merge = function (nums1, m, nums2, n) {
  const result = []
  let lastSmallestIndex = 0
  for (let i = 0; i < nums1.length; i++) {
    for (let j = lastSmallestIndex; j < nums2.length; j++) {
      if (nums1[i] > 0 && (nums1[i] < nums2[j] || nums2[j] === 0)) {
        result.push(nums1[i])
        if (i < nums1.length - 1) {
          break
        } else {
          continue
        }
      }

      if (nums2[j] <= nums1[i] || nums1[i] === 0) {
        result.push(nums2[j])
        lastSmallestIndex = j + 1
        continue
      }

      break
    }


    if (nums1[i] >= result[result.length - 1] && lastSmallestIndex >= nums2.length -1) {
      result.push(nums1[i])
    }
  }

  return result
};

var test_case_number = 1;
function equal(arr1, arr2) {
  if (arr1.length !== arr2.length) { return false }
  for (let i = 1; i < arr1.length; i++) {
    if (arr1[i] !== arr2[i]) {
      return false
    }
  }

  return true
}
function check(expected, output) {
  var result = equal(expected, output);
  var rightTick = "\u2713";
  var wrongTick = "\u2717";
  if (result) {
    var out = rightTick + ' Test #' + test_case_number;
    console.log(out);
  }
  else {
    var out = '';
    out += wrongTick + ' Test #' + test_case_number + ': Expected ';
    out += `${expected}`;
    out += ' Your output: ';
    out += `${output}`;
    console.log(out);
  }
  test_case_number++;
}

var input1 = [1, 2, 3, 0, 0, 0]
var input2 = [2, 5, 6]
var expected = [1, 2, 2, 3, 5, 6]
var output = merge(input1, input1.length, input2, input2.length)
check(expected, output)

var input1 = [1, 2, 3, 6, 7, 9, 0, 0]
var input2 = [2, 5, 6]
var expected = [1, 2, 2, 3, 5, 6, 6, 7, 9]
var output = merge(input1, input1.length, input2, input2.length)
check(expected, output)